export interface Contact {
  id?: number;
  name?: string;
  surname: string;
  tel: string;
  streetNumber?: number;
  address?: string;
  avatar?: string;
}
